import Vue from 'vue'
import Router from 'vue-router'
import Home from "../components/layout/Home";

Vue.use(Router)
const page = name => () => import('@/views/' + name,)


export default new Router({
  mode: 'history',
  routes: [
    {path: '/login', component: page('Login'), name: '登录'},

    //用户
    {
      path: '/', component: Home, name: 'Home',
      children: [
        //测试
        {path: '/test', component: page('test/Test'), name: '测试页面1'},
        //用户模块
        {path: '/usermanage', component: page('user-manage/index'), name: '用户模块'},
        //角色模块
        {path: '/rolemanage', component: page('role-manage/index'), name: '角色模块'},
        //用户角色管理模块
        {path: '/userrolemanage', component: page('user-role-manage/index'), name: '角色用户模块'},
      ]
    },
    //工单模块
    {
      path: '/', component: Home, name: '工单模块',
      children: [
        //工单管理
        {path: '/workflow', component: page('workflow-manage/index'), name: '工单'},
        //添加到审批
        {path: 'workflow/specificworkflow/:id/', component: page('specific-workflow-manage/index'), name: '审批流'},
      ]
    },
    //实例化工单
    {
      path: '/', component: Home, name: '实例化工单',
      children: [
        //工单管理
        {path: '/flow-conf-show', component: page('flow-conf-show-manage/index'), name: '新建工单'},
        {
          path: '/dynamicallygeneratedform/:id/',
          component: page('dynamically-generated-form-manage/index'),
          name: '动态生成表单'
        },
        {path: '/workorder', component: page('work-order-manage/index'), name: '展示工单'},
        {path: '/suborder', component: page('suborder-manage/index'), name: '展示子工单'},
        {path: '/examine-and-approve/:id/', component: page('examine-and-approve/index'), name: '审批'},

      ]
    },
  ]
})


import VueRouter from 'vue-router'
import index from "../components/index";
//import HelloWorld from '@/components/HelloWorld'
const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
