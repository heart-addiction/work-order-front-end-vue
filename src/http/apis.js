//将我们http.js中封装好的  get,post.put,delete  导过来
import {axios_get, axios_post, axios_delete, axios_put} from './index.js'

//登录接口
export const postLogin = (params, headers) => axios_post("/user/login/", params, headers)


//用户
//获取用户信息
export const getUser = (params, headers) => axios_get("/user/user/", params, headers)
//注册接口
export const postRegister = (params, headers) => axios_post("/user/register/", params, headers)
//删除
export const delUser = (params, headers) => axios_delete("/user/user/" + params.id + '/', headers)
//修改
export const upUser = (params, headers) => axios_put("/user/user/" + params.id + '/', params, headers)


//角色
//获取用户信息
export const getRole = (params, headers) => axios_get("/user/role/", params, headers)
//搜索
export const postRole = (params, headers) => axios_post("/user/search/", params, headers)
//删除
export const delRole = (params, headers) => axios_delete("/user/role/" + params.id + '/', headers)
//修改
export const upRole = (params, headers) => axios_put("/user/role/" + params.id + '/', params, headers)
//添加接口
export const postAddRole = (params, headers) => axios_post("/user/role/", params, headers)


//用户角色关联
//获取
export const getUserRole = (params, headers) => axios_get("/user/userrole/", params, headers)
//注册接口
export const postUserRole = (params, headers) => axios_post("/user/register/", params, headers)
//删除
export const delUserRole = (params, headers) => axios_delete("/user/userrole/" + params.id + '/', headers)
//修改
export const upUserRole = (params, headers) => axios_put("/user/userrole/" + params.id + '/', params, headers)
//添加接口
export const postUserAddRole = (params, headers) => axios_post("/user/userrole/", params, headers)


//工单模板
//获取信息
export const getFlowConf = (params, headers) => axios_get("/workflow/flowconf", params, headers)
//删除
export const delFlowConf = (params, headers) => axios_delete("/workflow/flowconf/" + params.id + '/', headers)
//修改
export const upFlowConf = (params, headers) => axios_put("/workflow/flowconf/" + params.id + '/', params, headers)
//添加接口
export const postFlowConf = (params, headers) => axios_post("/workflow/flowconf/", params, headers)
//获取单个的详细信息
export const getFlowConfDetails = (params, headers) => axios_get("/workflow/flowconf/" + params.id + '/', headers)

//审批流
//获取信息
export const getApproveActionConf = (params, headers) => axios_get("/workflow/approveactionconf/", params, headers)
//删除
export const delApproveActionConf = (params, headers) => axios_delete("/workflow/approveactionconf/" + params.id + '/', headers)
//修改
export const upApproveActionConf = (params, headers) => axios_put("/workflow/approveactionconf/" + params.id + '/', params, headers)
//添加接口
export const postApproveActionConf = (params, headers) => axios_post("/workflow/approveactionconf/", params, headers)


//实例化工单 子工单

//添加 实例化工单 子工单 接口
export const postWorkOrder = (params, headers) => axios_post("/order/workorder/", params, headers)
//获取信息
export const getWorkOrder = (params, headers) => axios_get("/order/workorders/", params, headers)
//删除
export const delWorkOrder = (params, headers) => axios_delete("/order/workorders/" + params.id + '/', headers)
//修改
export const upWorkOrder = (params, headers) => axios_put("/order/workorders/" + params.id + '/', params, headers)


//删除
export const delSubOrder = (params, headers) => axios_delete("/order/suborders/" + params.id + '/', headers)
//修改
export const upSubOrder = (params, headers) => axios_put("/order/suborders/", params, headers)
// http://192.168.56.100:8888/order/workorders/3/


//获取 工单以及当前工单下所有子工单 所有详细信息
export const getWorkOrderDeep = (params, headers) => axios_get("/order/workorders/" + params.id + '/', params, headers)

// 修改工单 子工单状态
export const upWorkorderSuborder = (params, headers) => axios_put("/order/workorder/", params, headers)

//获取过滤后当前登录用户的子工单
export const getSubOrder = (params, headers) => axios_get("/order/workorder/", params, headers)
export const getSubOrderPer = (params, headers) => axios_get("/order/suborderhasper/", params, headers)

// es搜索接口
export const getWorkSearch = (params, headers) => axios_get("/order/workorder_search/", params, headers)
// 工单搜索接口
export const postWorkSearch2 = (params, headers) => axios_post("/order/workordersearch/", params, headers)


export const postSubSearch = (params, headers) => axios_post("/order/subordersearch/", params, headers)

